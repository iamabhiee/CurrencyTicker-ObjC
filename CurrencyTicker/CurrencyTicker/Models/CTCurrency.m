//
//  CTCurrency.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrency.h"

@implementation CTCurrency

- (instancetype)initWithDataArray:(NSArray *)dataArray {
    if (self = [super init]) {
        self.currencyCode = dataArray[0];
        self.currencyName = dataArray[1];
        self.currencySymbol = dataArray[2];
    }
    return self;
}

+ (instancetype)currencyWithCode:(NSString *)code name:(NSString *)name symobol:(NSString *)symbol {
    CTCurrency *currency = [CTCurrency new];
    currency.currencyCode = code;
    currency.currencyName = name;
    currency.currencySymbol = symbol;
    
    return currency;
}

-(void)encodeWithCoder:(NSCoder*)encoder {
    [encoder encodeObject:self.currencyCode forKey:@"currencyCode"];
    [encoder encodeObject:self.currencyName forKey:@"currencyName"];
    [encoder encodeObject:self.currencySymbol forKey:@"currencySymbol"];
    [encoder encodeBool:self.isFavorite forKey:@"isFavorite"];
}

-(id)initWithCoder:(NSCoder*)decoder {
    self.currencyCode = [decoder decodeObjectForKey:@"currencyCode"];
    self.currencyName = [decoder decodeObjectForKey:@"currencyName"];
    self.currencySymbol = [decoder decodeObjectForKey:@"currencySymbol"];
    self.isFavorite = [decoder decodeBoolForKey:@"isFavorite"];
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    CTCurrency *currency = [[self class] currencyWithCode:self.currencyCode
                                  name:self.currencyName
                                   symobol:self.currencySymbol];
    
    currency.isFavorite = self.isFavorite;
    return currency;
}

- (NSUInteger)hash {
    return [self.currencyCode hash];
}

- (BOOL)isEqual:(id)object {
    return (self.hash == ((NSObject *)object).hash);
}

@end
