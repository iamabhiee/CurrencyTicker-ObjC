//
//  CTCurrencyComparision.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTCurrency;

@interface CTCurrencyExchange : NSObject

@property (nonatomic, strong) CTCurrency *baseCurrency;
@property (nonatomic, strong) CTCurrency *currency;

@property (nonatomic, strong) NSNumber *conversationRate;
@property (nonatomic, strong) NSNumber *previousConversationRate;

@property (nonatomic, strong) NSDate *date;

+ (instancetype)exchangeRateWithCurrency:(CTCurrency *)currency baseCurrency:(CTCurrency *)baseCurrency rate:(NSNumber *)rate date:(NSDate *)date;

- (NSString *)titleString;

- (NSString *)exchangeRateString;

- (BOOL)isDifferencePostive;
- (NSString *)differenceString;
- (NSString *)differencePercentageString;
@end
