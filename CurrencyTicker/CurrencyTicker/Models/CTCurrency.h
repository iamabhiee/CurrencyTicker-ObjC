//
//  CTCurrency.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTCurrency : NSObject

@property (nonatomic, strong) NSString *currencyName;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;
@property (nonatomic) BOOL isFavorite;

- (instancetype)initWithDataArray:(NSArray *)dataArray;

@end
