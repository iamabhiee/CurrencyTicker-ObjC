//
//  CTCurrencyComparision.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencyExchange.h"

#import "CTCurrency.h"

@implementation CTCurrencyExchange

+ (instancetype)exchangeRateWithCurrency:(CTCurrency *)currency baseCurrency:(CTCurrency *)baseCurrency rate:(NSNumber *)rate date:(NSDate *)date {
    CTCurrencyExchange *exchange = [CTCurrencyExchange new];
    exchange.currency = currency;
    exchange.baseCurrency = baseCurrency;
    exchange.conversationRate = rate;
    exchange.date = date;
    
    return exchange;
}

#pragma mark - Public methods

- (NSString *)titleString {
    return [NSString stringWithFormat:@"%@ - %@", self.baseCurrency.currencyCode, self.currency.currencyCode];
}

- (NSString *)exchangeRateString {
    float exchangeRate = [self.conversationRate floatValue];
    return [NSString stringWithFormat:@"%.4f", exchangeRate];
}

- (BOOL)isDifferencePostive {
    return ([self differenceValue] >= 0);
}

- (NSString *)differenceString {
    return [NSString stringWithFormat:@"%.4f", [self differenceValue]];
    
}

- (NSString *)differencePercentageString {
    return [NSString stringWithFormat:@"%.2f%@", [self differencePercentageValue], @"%"];
}

#pragma mark - Private methods

- (float)differenceValue {
    float exchangeRate = [self.conversationRate floatValue];
    float exchangeRatePreviousValue = [self.previousConversationRate floatValue];
    
    float difference = exchangeRate - exchangeRatePreviousValue;
    return difference;
}

- (float)differencePercentageValue {
    float exchangeRate = [self.conversationRate floatValue];
    
    if (exchangeRate > 0.0f) {
        float difference = [self differenceValue];
        float differencePer = (difference / exchangeRate) * 100;
        return differencePer;
    } else {
        return 0;
    }
    
}

@end
