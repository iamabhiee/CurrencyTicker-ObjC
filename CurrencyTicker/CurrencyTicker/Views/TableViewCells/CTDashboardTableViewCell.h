//
//  CTDashboardTableViewCell.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CTCurrencyExchange;

#define DASHBOARD_CELL @"CTDashboardTableViewCell"

@interface CTDashboardTableViewCell : UITableViewCell

- (void)configureWithExchangeData:(CTCurrencyExchange *)exchange;

@end
