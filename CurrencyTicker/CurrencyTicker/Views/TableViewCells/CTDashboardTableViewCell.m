//
//  CTDashboardTableViewCell.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTDashboardTableViewCell.h"

#import "CTCurrencyExchange.h"
#import "CTCurrency.h"

#import "CTCurrencyPriceChangeView.h"

@interface CTDashboardTableViewCell()

@property (nonatomic, weak) IBOutlet UIImageView *currencyImageView;
@property (nonatomic, weak) IBOutlet UILabel *lblcurrencyValue;
@property (nonatomic, weak) IBOutlet UILabel *lblcurrencyName;

@property (nonatomic, weak) IBOutlet CTCurrencyPriceChangeView *priceChangeContainerView;

@end

@implementation CTDashboardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithExchangeData:(CTCurrencyExchange *)exchange {
    CTCurrency *currency = exchange.currency;
    
    [self.currencyImageView setImage:[UIImage imageNamed:currency.currencyCode]];
    self.lblcurrencyName.text = currency.currencyCode;
    self.lblcurrencyValue.text = [exchange exchangeRateString];
    
    [self.priceChangeContainerView configureWithExchangeData:exchange];
}

@end
