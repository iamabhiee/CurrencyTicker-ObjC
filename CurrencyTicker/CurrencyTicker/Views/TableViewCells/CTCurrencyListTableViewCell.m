//
//  CTCurrencyListTableViewCell.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencyListTableViewCell.h"

#import "CTCurrency.h"

@interface CTCurrencyListTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *lblCurrencyName;
@property (nonatomic, weak) IBOutlet UILabel *lblCurrencyCode;
@property (nonatomic, weak) IBOutlet UILabel *lblCurrencySymbol;
@property (nonatomic, weak) IBOutlet UIImageView *countryImageView;

@end

@implementation CTCurrencyListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithCurrency:(CTCurrency *)currency {
    self.lblCurrencyName.text = currency.currencyName;
    self.lblCurrencyCode.text = currency.currencyCode;
    self.lblCurrencySymbol.text = currency.currencySymbol;
    [self.countryImageView setImage:[UIImage imageNamed:currency.currencyCode]];
}

@end
