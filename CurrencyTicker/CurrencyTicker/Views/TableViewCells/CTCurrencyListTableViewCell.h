//
//  CTCurrencyListTableViewCell.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTCurrency;

#define CURRECNY_LIST_CELL @"CTCurrencyListTableViewCell"

@interface CTCurrencyListTableViewCell : UITableViewCell

- (void)configureWithCurrency:(CTCurrency *)currency;

@end
