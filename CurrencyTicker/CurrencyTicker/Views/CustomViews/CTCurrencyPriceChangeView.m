//
//  CTCurrencyPriceChangeView.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencyPriceChangeView.h"

#import "CTCurrencyExchange.h"

#import "UIColor+CTColors.h"

@interface CTCurrencyPriceChangeView()

@property (nonatomic, weak) IBOutlet UILabel *lblcurrencyChangeValue;
@property (nonatomic, weak) IBOutlet UILabel *lblcurrencyChangePercentage;

@end

@implementation CTCurrencyPriceChangeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)configureWithExchangeData:(CTCurrencyExchange *)exchange {
    self.lblcurrencyChangeValue.text = [exchange differenceString];
    self.lblcurrencyChangePercentage.text = [exchange differencePercentageString];
    
    self.backgroundColor = [exchange isDifferencePostive] ? [UIColor ct_greenColor] : [UIColor ct_redColor];
}

@end
