//
//  CTCurrencyPriceChangeView.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTCurrencyExchange;

@interface CTCurrencyPriceChangeView : UIView

- (void)configureWithExchangeData:(CTCurrencyExchange *)exchange;

@end
