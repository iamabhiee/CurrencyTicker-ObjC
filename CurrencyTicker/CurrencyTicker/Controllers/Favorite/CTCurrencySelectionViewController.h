//
//  FTCountrySelectionViewController.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTCurrency;

typedef NS_ENUM(NSInteger, CTCurrencySelectionViewControllerType) {
    CTCurrencySelectionViewControllerTypeBaseCurrency,
    CTCurrencySelectionViewControllerTypeFavoriteCurrencies
};


@class CTCurrencySelectionViewController;

@protocol CTCurrencySelectionViewControllerDelegate <NSObject>

- (void)currencySelectionViewController:(CTCurrencySelectionViewController *)controller selectCurrencies:(NSArray *)currencies;

@end

@interface CTCurrencySelectionViewController : UIViewController

@property (nonatomic) CTCurrencySelectionViewControllerType type;

@property (nonatomic, weak) id<CTCurrencySelectionViewControllerDelegate> delegate;

@property (nonatomic, copy) NSArray *currencyData;
@property (nonatomic) NSInteger selectedCurrencyIndex;

@end
