//
//  FTCountrySelectionViewController.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencySelectionViewController.h"

#import "CTCurrency.h"
#import "CTCurrencyManager.h"

#import "UITableView+Utilities.h"
#import "CTCurrencyListTableViewCell.h"

@interface CTCurrencySelectionViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation CTCurrencySelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Register custom cells
    [self.tableView registerXIB:@[CURRECNY_LIST_CELL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - IBAction

- (IBAction)actionSave:(id)sender {
    if (self.type == CTCurrencySelectionViewControllerTypeBaseCurrency) {
        CTCurrency *currency = self.currencyData[self.selectedCurrencyIndex];
        [self.delegate currencySelectionViewController:self selectCurrencies:@[currency]];
    } else {
        [self.delegate currencySelectionViewController:self selectCurrencies:self.currencyData];
    }
}

- (IBAction)actionClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.currencyData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CTCurrencyListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CURRECNY_LIST_CELL forIndexPath:indexPath];
    CTCurrency *currency = [self currencyAtIndexPath:indexPath];
    [cell configureWithCurrency:currency];
    
    if ([self isCurrencySelectedAtIndexPath:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CTCurrency *currency = [self currencyAtIndexPath:indexPath];
    NSArray *indexPathsToReload = @[indexPath];
    if (self.type == CTCurrencySelectionViewControllerTypeFavoriteCurrencies) {
        currency.isFavorite = !currency.isFavorite;
    } else if (self.type == CTCurrencySelectionViewControllerTypeBaseCurrency) {
        NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:self.selectedCurrencyIndex inSection:indexPath.section];
        indexPathsToReload = [indexPathsToReload arrayByAddingObject:previousIndexPath];
        self.selectedCurrencyIndex = indexPath.row;
    }
    
    [tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Currency manager

- (CTCurrency *)currencyAtIndexPath:(NSIndexPath *)indexPath {
    return self.currencyData[indexPath.row];
}

#pragma mark - Private methods

- (BOOL)isCurrencySelectedAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type == CTCurrencySelectionViewControllerTypeBaseCurrency) {
        return (indexPath.row == self.selectedCurrencyIndex);
    } else {
        CTCurrency *currency = [self currencyAtIndexPath:indexPath];
        return currency.isFavorite;
    }
}

@end
