//
//  CTCurrencyExchangeDetailsViewController.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 14/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencyExchangeDetailsViewController.h"

#import "NSDate+Utilities.h"
#import "UIColor+CTColors.h"
#import "CTCurrency.h"
#import "CTCurrencyExchange.h"
#import "CTExchangeDataManager.h"

#import "UUChart.h"
#import "CTCurrencyPriceChangeView.h"

#import "UIViewController+Utilities.h"

@interface CTCurrencyExchangeDetailsViewController () <UUChartDataSource>

@property (nonatomic, weak) IBOutlet UIView *myGraph;
@property (nonatomic, weak) IBOutlet UILabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyCode;

@property (nonatomic, weak) IBOutlet CTCurrencyPriceChangeView *priceChangeContainerView;

@property (nonatomic, weak) IBOutlet UILabel *lblWeeklyHighValue;
@property (nonatomic, weak) IBOutlet UILabel *lblWeeklyLowValue;


@property (nonatomic, strong) NSMutableArray *pastData;

@property (strong, nonatomic) NSMutableArray *xAxisData;
@property (strong, nonatomic) NSMutableArray *yAxisData;

@property (nonatomic, strong) UUChart *chartView;

@end

@implementation CTCurrencyExchangeDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Graph
    self.chartView = [[UUChart alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, CGRectGetHeight(self.myGraph.frame)-20)
                                   dataSource:self
                                        style:UUChartStyleLine];
    
    //Data
    self.xAxisData = [NSMutableArray new];
    self.yAxisData = [NSMutableArray new];
    
    //Title
    self.title = [self.selectedCurrencyExchange titleString];
    self.lblCurrencyCode.text = self.selectedCurrencyExchange.currency.currencyCode;
    
    //Current Values
    [self.priceChangeContainerView configureWithExchangeData:self.selectedCurrencyExchange];
    
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - 

- (void)fetchData {
    [self showProgressView];
    [CTExchangeDataManager fetchPastWeekExchangeDataForCurrency:self.selectedCurrencyExchange.currency baseCurrency:self.selectedCurrencyExchange.baseCurrency success:^(id responseObject) {
        [self hideProgressView];
        
        self.pastData = [NSMutableArray arrayWithArray:responseObject];
        
        //Axes data
        for (CTCurrencyExchange *exchangeData in self.pastData) {
            [self.yAxisData addObject:@([exchangeData.conversationRate floatValue])];
            [self.xAxisData addObject:[exchangeData.date shortStringValue]];
        }
        
        //Set Weekly Low High
        CGRange valueRange = [self valueRange];
        self.lblWeeklyLowValue.text = [NSString stringWithFormat:@"%.4f", valueRange.min];
        self.lblWeeklyHighValue.text = [NSString stringWithFormat:@"%.4f", valueRange.max];
        
        [self.chartView showInView:self.myGraph];
    } failure:^(NSError *error) {
        [self handleError:error];
    }];
}

#pragma mark - @required

- (NSArray *)chartConfigAxisXLabel:(UUChart *)chart {
    return self.xAxisData;
}

- (NSArray *)chartConfigAxisYValue:(UUChart *)chart {
    return @[self.yAxisData];
}

#pragma mark - @optional
- (NSArray *)chartConfigColors:(UUChart *)chart {
    return @[[UIColor ct_blueColor]];
}

- (CGRange)chartRange:(UUChart *)chart {
    return [self valueRange];
}

#pragma mark optional

- (CGRange)chartHighlightRangeInLine:(UUChart *)chart {
    return CGRangeZero;
}

- (BOOL)chart:(UUChart *)chart showHorizonLineAtIndex:(NSInteger)index {
    return YES;
}

- (BOOL)chart:(UUChart *)chart showMaxMinAtIndex:(NSInteger)index {
    return NO;
}

#pragma mark - Data

- (CGRange)valueRange {
    float xmax = -MAXFLOAT;
    float xmin = MAXFLOAT;
    for (NSNumber *num in self.yAxisData) {
        float x = num.floatValue;
        if (x < xmin) xmin = x;
        if (x > xmax) xmax = x;
    }
    
    return CGRangeMake(xmax, xmin);
}

@end
