//
//  CTCurrencyExchangeDetailsViewController.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 14/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTCurrencyExchange;

@interface CTCurrencyExchangeDetailsViewController : UIViewController

@property (nonatomic, strong) CTCurrencyExchange *selectedCurrencyExchange;

@end
