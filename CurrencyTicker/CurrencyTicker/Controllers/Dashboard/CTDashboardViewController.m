//
//  CTDashboardViewController.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTDashboardViewController.h"

#import "CTDashboardTableViewCell.h"

#import "CTCurrency.h"
#import "CTCurrencyExchange.h"
#import "CTCurrencyManager.h"
#import "CTExchangeDataManager.h"

#import "UITableView+Utilities.h"

#import "UIViewController+Utilities.h"
#import "CTCurrencyExchangeDetailsViewController.h"
#import "CTCurrencySelectionViewController.h"

#define FAVORITE_SELECTION_SEGUE @"showFavoriteSelection"
#define EXCHANGE_DETAILS_SEGUE @"showExchangeDetails"

@interface CTDashboardViewController () <UITableViewDataSource, UITableViewDelegate, CTCurrencySelectionViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnDefaultcurrency;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) NSArray *currencyExchangeData;

@end

@implementation CTDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Empty back button
    [self addEmptyBackButton];
    
    //Register custom cells
    [self.tableView registerXIB:@[DASHBOARD_CELL]];
    [self.tableView hideEmptyRows];
    
    
    //Default currency
    [self updateBaseCurrencyTitle];
    
    //Pull to refresh
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.tableView sendSubviewToBack:self.refreshControl];
    
    //Fetch currency data
    [self showProgressView];
    [self reloadCurrencyData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:EXCHANGE_DETAILS_SEGUE]) {
        CTCurrencyExchangeDetailsViewController *controller = [segue destinationViewController];
        controller.selectedCurrencyExchange = [self currencyAtIndexPath:self.tableView.indexPathForSelectedRow];
    } else if ([segue.identifier isEqualToString:FAVORITE_SELECTION_SEGUE]) {
        UINavigationController *nav = segue.destinationViewController;
        CTCurrencySelectionViewController *controller = [[nav viewControllers] firstObject];
        controller.type = ((UIButton *)sender).tag;
        NSArray *allCurrencies = [[NSArray alloc] initWithArray:[[CTCurrencyManager sharedInstance] allCurrencies] copyItems:YES];
        controller.currencyData = allCurrencies;
        
        NSInteger selectedIndex = [allCurrencies indexOfObject:[[CTCurrencyManager sharedInstance] getDefaultCurrency]];
        
        controller.selectedCurrencyIndex = selectedIndex;
        controller.delegate = self;
    }
}

#pragma mark - IBActions

- (IBAction)actionFavorites:(id)sender {
    [self performSegueWithIdentifier:FAVORITE_SELECTION_SEGUE sender:sender];
}

- (IBAction)actionChangeDefaultCurrency:(id)sender {
    [self performSegueWithIdentifier:FAVORITE_SELECTION_SEGUE sender:sender];
}

- (IBAction)handleRefresh:(id)sender {
    [self reloadCurrencyData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.currencyExchangeData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CTDashboardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DASHBOARD_CELL forIndexPath:indexPath];
    CTCurrencyExchange *currencyExchange = [self currencyAtIndexPath:indexPath];
    [cell configureWithExchangeData:currencyExchange];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:EXCHANGE_DETAILS_SEGUE sender:self];
}

#pragma mark - Currency manager

- (CTCurrencyExchange *)currencyAtIndexPath:(NSIndexPath *)indexPath {
    return self.currencyExchangeData[indexPath.row];
}

#pragma mark - CTCurrencySelectionViewControllerDelegate

- (void)currencySelectionViewController:(CTCurrencySelectionViewController *)controller selectCurrencies:(NSArray *)currencies {
    if (controller.type == CTCurrencySelectionViewControllerTypeBaseCurrency) {
        CTCurrency *defaultCurrency = [currencies firstObject];
        [[CTCurrencyManager sharedInstance] saveDefaultCurrency:defaultCurrency];
        [self updateBaseCurrencyTitle];
    } else if (controller.type == CTCurrencySelectionViewControllerTypeFavoriteCurrencies) {
        [[CTCurrencyManager sharedInstance] saveCurrencies:currencies];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self showProgressView];
        [self reloadCurrencyData];
    }];
}

#pragma mark - Private methods

- (void)updateBaseCurrencyTitle {
    NSString *defaultCurrencyTitle = [NSString stringWithFormat:@"%@ ▿", [[CTCurrencyManager sharedInstance] getDefaultCurrency].currencyCode];
    self.btnDefaultcurrency.title = defaultCurrencyTitle;
}

- (void)reloadCurrencyData {
    self.currencyExchangeData = nil;
    
    NSArray *favoriteCurrencies = [[CTCurrencyManager sharedInstance] favoutiteCurrencies];
    [CTExchangeDataManager fetchLatestExchangeDataWithDifferenceForCurrencies:favoriteCurrencies baseCurrency:[[CTCurrencyManager sharedInstance] getDefaultCurrency] success:^(id responseObject) {
        self.currencyExchangeData = [NSArray arrayWithArray:responseObject];
        [self stopLoadAndRefreshData];
    } failure:^(NSError *error) {
        [self stopLoadAndRefreshData];
        [self handleError:error];
    }];
}

- (void)stopLoadAndRefreshData {
    [self hideProgressView];
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

@end
