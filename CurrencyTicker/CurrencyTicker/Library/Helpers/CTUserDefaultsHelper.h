//
//  CTUserDefaultsHelper.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTUserDefaultsHelper : NSObject

+ (void)saveCustomObject:(id)object key:(NSString *)key;
+ (id)loadCustomObjectWithKey:(NSString *)key;

@end
