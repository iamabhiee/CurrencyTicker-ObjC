//
//  NSDate+Utilities.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 14/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

+ (NSDate *)dateFromString:(NSString *)string;

- (NSDate *)dateAfterDays:(NSInteger)days;

- (NSString *)stringValue;
- (NSString *)shortStringValue;

@end
