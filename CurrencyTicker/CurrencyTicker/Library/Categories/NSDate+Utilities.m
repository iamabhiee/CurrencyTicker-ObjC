//
//  NSDate+Utilities.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 14/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "NSDate+Utilities.h"

@implementation NSDate (Utilities)

+ (NSDate *)dateFromString:(NSString *)string {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter dateFromString:string];
}

- (NSDate *)dateAfterDays:(NSInteger)days {
    return [self dateByAddingTimeInterval:(days * 60 * 60 * 24)];
}

- (NSString *)stringValue {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:self];
}

- (NSString *)shortStringValue {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd"];
    return [formatter stringFromDate:self];
}

@end
