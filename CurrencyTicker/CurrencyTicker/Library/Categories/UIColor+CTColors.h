//
//  UIColor+CTColors.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CTColors)

+ (UIColor *)ct_blueColor;
+ (UIColor *)ct_redColor;
+ (UIColor *)ct_greenColor;

@end
