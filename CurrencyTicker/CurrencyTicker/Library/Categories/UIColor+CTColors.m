//
//  UIColor+CTColors.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "UIColor+CTColors.h"

@implementation UIColor (CTColors)

+ (UIColor *)ct_blueColor {
    return [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
}

+ (UIColor *)ct_redColor {
    return [UIColor colorWithRed:198.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
}

+ (UIColor *)ct_greenColor {
    return [UIColor colorWithRed:46.0/255.0 green:125.0/255.0 blue:50.0/255.0 alpha:1.0];
}

@end
