//
//  UITableView+Utilities.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Utilities)

- (void)registerXIB:(NSArray *)xibNames;
- (void)hideEmptyRows;

@end
