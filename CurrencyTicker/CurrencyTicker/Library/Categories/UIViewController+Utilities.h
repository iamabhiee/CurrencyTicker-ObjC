//
//  UIViewController+Utilities.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utilities)

//Navigation bar
- (void)addEmptyBackButton;

//Progress View
- (void)showProgressView;
- (void)hideProgressView;

//Error handling
- (void)handleError:(NSError *)error;



@end
