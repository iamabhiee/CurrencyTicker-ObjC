//
//  UITableView+Utilities.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "UITableView+Utilities.h"

@implementation UITableView (Utilities)

- (void)registerXIB:(NSArray *)xibNames {
    for (NSString *cellIdentifier in xibNames) {
        [self registerNib:[UINib nibWithNibName:cellIdentifier bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
    }
}

- (void)hideEmptyRows {
    self.tableFooterView = [[UIView alloc] init];
}

@end
