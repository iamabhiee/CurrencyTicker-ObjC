//
//  UIViewController+Utilities.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 16/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "UIViewController+Utilities.h"

#import "MBProgressHUD.h"

@implementation UIViewController (Utilities)

- (void)addEmptyBackButton {
    //Back button
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
}

- (void)showProgressView {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.view bringSubviewToFront:hud];
}

- (void)hideProgressView {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)handleError:(NSError *)error {
    [self hideProgressView];
    
    if (error) {
        [self showAlertWithTitle:@"Error!" message:error.localizedDescription];
    }
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
