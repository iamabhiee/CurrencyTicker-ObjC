//
//  CTNetworkingManager.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 13/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface CTNetworkingManager : AFHTTPSessionManager

+ (instancetype)sharedInstance;

- (void)get:(NSString *)path params:(NSDictionary *)params success:(void (^)(id responseObject))success
    failure:(void (^)(NSError *error))failure;

@end
