//
//  CTNetworkingManager.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 13/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTNetworkingManager.h"

#define BASE_URL @"http://api.fixer.io/"

@implementation CTNetworkingManager

#pragma mark - Init

+ (instancetype)sharedInstance {
    static CTNetworkingManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[CTNetworkingManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
    });
    return _sharedInstance;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    
    if(self) {
        [self.requestSerializer setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
        
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

#pragma mark - Public methods

- (void)get:(NSString *)path params:(NSDictionary *)params success:(void (^)(id responseObject))success
    failure:(void (^)(NSError *error))failure {
    NSLog(@"Request : %@ Params : %@", path, params);
    
    [self GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

@end
