//
//  CTCurrencyDataManager.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTCurrency;

@interface CTCurrencyLocalDataRepository : NSObject

+ (NSArray *)currenciesFromJSON;

+ (NSArray *)storedCurrencies;
+ (void)saveCurrencies:(NSArray *)currencies;

+ (CTCurrency *)defaultCurrency;
+ (void)saveDefaultCurrency:(CTCurrency *)currency;

@end
