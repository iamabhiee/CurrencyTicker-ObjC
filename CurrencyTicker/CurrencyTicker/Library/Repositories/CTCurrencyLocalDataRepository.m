//
//  CTCurrencyDataManager.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 11/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencyLocalDataRepository.h"

#import "CTUserDefaultsHelper.h"
#import "CTCurrency.h"

#define CURRENCIES_KEY @"currencies"
#define DEFAULT_CURRENCY_KEY @"defaultCurrency"

@implementation CTCurrencyLocalDataRepository

+ (NSArray *)currenciesFromJSON {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"currencies" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error;
    NSArray *currencyJsonDataArray = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&error];
    
    NSMutableArray *currencyData = [NSMutableArray new];
    for (NSArray *currencyArray in currencyJsonDataArray) {
        CTCurrency *currency = [[CTCurrency alloc] initWithDataArray:currencyArray];
        [currencyData addObject:currency];
    }
    return [currencyData copy];
}

+ (NSArray *)storedCurrencies {
    return [CTUserDefaultsHelper loadCustomObjectWithKey:CURRENCIES_KEY];
}

+ (void)saveCurrencies:(NSArray *)currencies {
    [CTUserDefaultsHelper saveCustomObject:currencies key:CURRENCIES_KEY];
}

+ (CTCurrency *)defaultCurrency {
    return [CTUserDefaultsHelper loadCustomObjectWithKey:DEFAULT_CURRENCY_KEY];
}

+ (void)saveDefaultCurrency:(CTCurrency *)currency {
    [CTUserDefaultsHelper saveCustomObject:currency key:DEFAULT_CURRENCY_KEY];
}

@end
