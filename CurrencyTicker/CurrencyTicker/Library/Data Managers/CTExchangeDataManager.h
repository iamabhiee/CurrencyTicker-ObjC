//
//  CTExchangeDataManager.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 12/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTCurrency;

@interface CTExchangeDataManager : NSObject

+ (void)fetchLatestExchangeDataWithDifferenceForCurrencies:(NSArray *)currencies baseCurrency:(CTCurrency *)baseCurrency success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

+ (void)fetchPastWeekExchangeDataForCurrency:(CTCurrency *)currency baseCurrency:(CTCurrency *)baseCurrency success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

@end
