//
//  CTCurrencyManager.h
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 13/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTCurrency;

@interface CTCurrencyManager : NSObject

+ (CTCurrencyManager *)sharedInstance;

- (NSArray*)allCurrencies;
- (NSArray*)favoutiteCurrencies;
- (void)saveCurrencies:(NSArray *)currencies;

- (CTCurrency *)getDefaultCurrency;
- (void)saveDefaultCurrency:(CTCurrency *)defaultCurrency;

@end
