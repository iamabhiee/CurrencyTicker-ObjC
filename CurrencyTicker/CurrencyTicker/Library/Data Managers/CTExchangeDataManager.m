//
//  CTExchangeDataManager.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 12/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTExchangeDataManager.h"

#import "NSDate+Utilities.h"
#import "CTNetworkingManager.h"
#import "CTCurrency.h"
#import "CTCurrencyExchange.h"

@implementation CTExchangeDataManager

+ (void)fetchLatestExchangeDataWithDifferenceForCurrencies:(NSArray *)currencies baseCurrency:(CTCurrency *)baseCurrency success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    NSDate *today = [NSDate date];
    [self fetchExchangeDataForCurrencies:currencies baseCurrency:baseCurrency date:today success:^(id responseObject, NSDate *date) {
        NSArray *firstData = [NSMutableArray arrayWithArray:responseObject];
        NSDate *previousDate = [date dateAfterDays:-1];
        
        [self fetchExchangeDataForCurrencies:currencies baseCurrency:baseCurrency date:previousDate success:^(id responseObject, NSDate *date) {
            for (int i = 0; i < firstData.count; i++) {
                CTCurrencyExchange *firstDate = firstData[i];
                CTCurrencyExchange *secondDate = responseObject[i];
                
                firstDate.previousConversationRate = secondDate.conversationRate;
            }
            success(firstData);
        } failure:failure];
    } failure:failure];
}

+ (void)fetchPastWeekExchangeDataForCurrency:(CTCurrency *)currency baseCurrency:(CTCurrency *)baseCurrency success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    [self fetchRecursiveDataForCurrency:currency baseCurrency:baseCurrency days:7 date:[NSDate date] data:[NSMutableArray new] success:success failure:failure];
}

#pragma mark - Private methods

+ (void)fetchExchangeDataForCurrencies:(NSArray *)currencies baseCurrency:(CTCurrency *)baseCurrency date:(NSDate *)date success:(void (^)(id responseObject, NSDate *date))success failure:(void (^)(NSError *error))failure {
    //Date
    NSString *dateString = [date stringValue];
    
    //Currencies
    NSString *joinedCurrencies;
    for (CTCurrency *currency in currencies) {
        //Append Comma
        if (!joinedCurrencies) {
            joinedCurrencies = @"";
        } else {
            joinedCurrencies = [joinedCurrencies stringByAppendingFormat:@","];
        }
        
        joinedCurrencies = [joinedCurrencies stringByAppendingFormat:@"%@", currency.currencyCode];
    }
    
    NSString *apiURL = [NSString stringWithFormat:@"%@?symbols=%@&base=%@", dateString, joinedCurrencies, baseCurrency.currencyCode];
    [[CTNetworkingManager sharedInstance] get:apiURL params:@{} success:^(id responseObject) {
        NSDictionary *rates = responseObject[@"rates"];
        NSDate *date = [NSDate dateFromString:responseObject[@"date"]];
        NSMutableArray *exchangeData = [NSMutableArray new];
        for (CTCurrency *c in currencies) {
            NSNumber *rate = rates[c.currencyCode];
            CTCurrencyExchange *exchange = [CTCurrencyExchange exchangeRateWithCurrency:c baseCurrency:baseCurrency rate:rate date:date];
            [exchangeData addObject:exchange];
        }
        success(exchangeData, date);
    } failure:failure];
}

+ (void)fetchRecursiveDataForCurrency:(CTCurrency *)currency baseCurrency:(CTCurrency *)baseCurrency days:(NSInteger)days date:(NSDate *)startDate data:(NSMutableArray *)existingData success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    __block NSInteger remainingDays = days-1;
    [self fetchExchangeDataForCurrencies:@[currency] baseCurrency:baseCurrency date:startDate success:^(id responseObject, NSDate *date) {
        [existingData addObjectsFromArray:responseObject];
        
        NSDate *previousDay = [date dateAfterDays:-1];
        if (remainingDays == 0) {
            NSArray *orderedData = [[existingData reverseObjectEnumerator] allObjects];
            success(orderedData);
        } else {
            [self fetchRecursiveDataForCurrency:currency baseCurrency:baseCurrency days:remainingDays date:previousDay data:existingData success:success failure:failure];
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
