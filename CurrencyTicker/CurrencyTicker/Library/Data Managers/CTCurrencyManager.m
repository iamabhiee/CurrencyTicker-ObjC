//
//  CTCurrencyManager.m
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 13/07/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "CTCurrencyManager.h"

#import "CTCurrency.h"
#import "CTCurrencyLocalDataRepository.h"

@interface CTCurrencyManager()

@property (nonatomic, strong) NSArray *currencies;
@property (nonatomic, strong) CTCurrency *defaultCurrency;

@end

@implementation CTCurrencyManager

#pragma mark - Singleton

+ (CTCurrencyManager *)sharedInstance {
    static dispatch_once_t oncePredicate;
    static CTCurrencyManager* _sharedInstance = nil;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [self alloc];
    });
    return _sharedInstance;
}

- (CTCurrency *)getDefaultCurrency {
    return self.defaultCurrency;
}

- (NSArray *)allCurrencies {
    return self.currencies;
}

- (NSArray *)favoutiteCurrencies {
    NSMutableArray *favoriteCurrencies = [NSMutableArray new];
    for (CTCurrency *currency in self.currencies) {
        if (currency.isFavorite) {
            [favoriteCurrencies addObject:currency];
        }
    }
    
    return favoriteCurrencies;
}

- (void)saveCurrencies:(NSArray *)currencies {
    _currencies = [currencies copy];
    [CTCurrencyLocalDataRepository saveCurrencies:currencies];
}

- (void)saveDefaultCurrency:(CTCurrency *)defaultCurrency {
    [CTCurrencyLocalDataRepository saveDefaultCurrency:defaultCurrency];
    _defaultCurrency = defaultCurrency;
}

#pragma mark - Accessors 

- (NSArray *)currencies {
    if (!_currencies) {
        _currencies = [CTCurrencyLocalDataRepository storedCurrencies];
        if (!_currencies) {
            NSArray *storedCurrencies = [CTCurrencyLocalDataRepository currenciesFromJSON];
            [self saveCurrencies:storedCurrencies];
        }
    }
    
    return _currencies;
}

- (CTCurrency *)defaultCurrency {
    if (!_defaultCurrency) {
        //Get default currency from user default
        _defaultCurrency = [CTCurrencyLocalDataRepository defaultCurrency];
        
        if (!_defaultCurrency) {
            //Get default currency from locale
            NSLocale *theLocale = [NSLocale currentLocale];
            NSString *code = [theLocale objectForKey:NSLocaleCurrencyCode];
            
            _defaultCurrency = [self currencyForCode:code];
            if (!_defaultCurrency) {
                //USD currency
                _defaultCurrency = [self currencyForCode:@"USD"];
            }
            
            [self saveDefaultCurrency:_defaultCurrency];
        }
    }
    
    return _defaultCurrency;
}

#pragma mark - Private methods

- (CTCurrency *)currencyForCode:(NSString *)code {
    for (CTCurrency *currency in self.allCurrencies) {
        if ([currency.currencyCode isEqualToString:code]) {
            return currency;
        }
    }
    
    return nil;
}

@end
