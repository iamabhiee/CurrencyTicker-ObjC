# Currency Ticker #

Repository for Currency Ticker, iOS dev quest. The project implements Currency Ticker based on data provided by fixer.io. UI is designed by storyboards and xibs (UITableViewCell). Networking is handled with AFNetworking. App will work on any device using iOS8+. This project is entirly written in Objective-C. Cocoapods is used to manage dependencies.

### Functionalities ###

* Dashboard : User can track rate of their favorite currencies
* Currency Detail View :­ Shown after tapping on currency in dashboard. It displays historic data of past 7 days with 7 days high and low price.
* Favorites Manager ­: User can add/edit/remove favorite currency tickers.
* Base Currency : User can track other currencies against a base currency, By default base currency is based on User's Locale

### Extras ###
* Data caching is used to get faster data
* Once data is fetched, Data can be also accesed without network connection

### Installation ###
This project is made with latest version of XCode(7.3.1). To run the project 
* Download latest source code from "master" branch 
* Install dependency by running "pod install"
* Open "CurrencyTicker.xcworkspace" in XCode